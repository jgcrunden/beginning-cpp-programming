#include <cstring>
#include <iostream>
#include "Mystring.h"

// No-args constructor
Mystring::Mystring() 
    : str{nullptr} {
        str = new char[1];
        *str = '\0';
}
// Overloaded constructor
Mystring::Mystring(const char *s)
    :str {nullptr} {
        if (s == nullptr) {
            str = new char[1];
            *str = '\0';
        } else {
            str = new char[std::strlen(s) + 1];
            std::strcpy(str, s);
        }
}

// Copy constructor
Mystring::Mystring(const Mystring &source)
    : str {nullptr} {
      str = new char[std::strlen(source.str) + 1];
      std::strcpy(str, source.str);
}

// Move constructor (not assignment)
Mystring::Mystring(Mystring &&source)
    :str{source.str} {
        source.str = nullptr;
        std::cout << "Move constructor" << std::endl;
} 

//Copy assignment
Mystring &Mystring::operator=(const Mystring &rhs) {
    std::cout << "Copy assignment" << std::endl;
    if(this == &rhs) {
        return *this;
    }
    delete [] this->str;
    str = new char [std::strlen(rhs.str) +1];
    std::strcpy(this->str, rhs.str);
    return *this;
}

// Move assignment
//Mystring &Mystring::operator =(Mystring &&rhs) {
//    std::cout << "Using move assignment" << std::endl;
//    if(this == &rhs)
//        return *this;
//    delete [] str;
//    str = rhs.str;
//    rhs.str = nullptr;
//    return *this;
//}

// Make lowercase
//Mystring Mystring::operator-() const {
//    char *buff = new char[std::strlen(str) +1];
//    std::strcpy(buff, str);
//    for(size_t i{0}; i < std::strlen(buff); i++) {
//        buff[i] = std::tolower(buff[i]);
//    }
//    Mystring temp {buff};
//    delete [] buff;
//    return temp;
//}

// Concatenate overload
//Mystring Mystring::operator +(const Mystring &rhs) const {
//    char *buff = new char[std::strlen(str) + std::strlen(rhs.str) + 1];
//    std::strcpy(buff, str);
//    std::strcat(buff, rhs.str);
//    Mystring temp {buff};
//    delete [] buff;
//    return temp;
//}

// Overloaded equality
//bool Mystring::operator==(const Mystring &rhs) const {
//    return (std::strcmp(str, rhs.str) == 0);
//}

// Destructor
Mystring::~Mystring() {
//    if(str == nullptr) {
//       std::cout << "Calling destructor for Mystring: nullptr" << std::endl; 
//    } else {
//        std::cout << "Calling destructor for Mystring: " << str << std::endl; 
//    }
    
    delete [] str;
}

// Display method
void Mystring::display() const {
    std::cout << str << ": " << get_length() << std::endl;
}

//length getter
int Mystring::get_length() const { return std::strlen(str); }

// string getter
const char *Mystring::get_str() const { return str; }

///////////////////////////
// Non-member functions
///////////////////////////

// equality
bool operator==(const Mystring &lhs, const Mystring &rhs) {
    return (std::strcmp(lhs.str, rhs.str) == 0);
}

// lowercase
Mystring operator-(const Mystring &obj) {
    char *buff = new char[std::strlen(obj.str) +1];
    std::strcpy(buff, obj.str);
    for(size_t i{0}; i < std::strlen(buff); i++) {
        buff[i] = std::tolower(buff[i]);
    }
    
    Mystring temp {buff};
    delete [] buff;
    return temp;
}

// Concatenation
Mystring operator+(const Mystring &lhs, const Mystring &rhs) {
    char *buff = new char[std::strlen(lhs.str) + std::strlen(rhs.str) + 1];
    std::strcpy(buff, lhs.str);
    std::strcat(buff, rhs.str);
    Mystring temp {buff};
    delete [] buff;
    return temp;
}

// Overloaded insertion
std::ostream &operator<<(std::ostream &os, const Mystring &rhs) {
    os << rhs.str;
    return os;
}

// Extraction overload
std::istream &operator>>(std::istream &is, Mystring &rhs) {
    char *buff = new char[1000];
    is >> buff;
    rhs = Mystring{buff};
    delete [] buff;
    return is;

}