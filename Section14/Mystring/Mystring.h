#ifndef MYSTRING_H
#define MYSTRING_H

class Mystring {
    friend bool operator==(const Mystring &lhs, const Mystring &rhs);
    friend Mystring operator-(const Mystring &obj);
    friend Mystring operator+(const Mystring &lhs, const Mystring &rhs);
    friend std::ostream &operator<<(std::ostream &os, const Mystring &rhs);
    friend std::istream &operator>>(std::istream &is, Mystring &rhs);
private:
    char *str;
    int myInt;
public:
    // constructors
    Mystring();
    Mystring(const char *s);
    // Copy Constructor
    Mystring(const Mystring &source);
    // move constructor operator
    Mystring(Mystring &&source);
    // copy Assignment operator
    // & refers to the return value Mystring, it is return a reference to the object passed in
    Mystring &operator=(const Mystring &rhs);
    // move assignment operator
//    Mystring &operator=(Mystring &&rhs);
//    // Overloaded negation - make lowercase of string
//    // No & for Mystring because we do not want to affect the object passed in
//    Mystring operator-() const;
//    // Concat
//    Mystring operator+(const Mystring &rhs) const;
//    // Overloaded comparison
//    bool operator==(const Mystring &rhs) const;
//    // Destructor
    ~Mystring();
    // Methods
    void display() const;
    // Returns current length of string
    int get_length() const;
    // Returns pointer to the string
    const char *get_str() const;

};

#endif // MYSTRING_H
