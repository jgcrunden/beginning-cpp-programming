#include <iostream>
#include "Mystring.h"

using namespace std;

int main() {
    cout << boolalpha << endl;

//    b = a; // calls overloaded copy assignment operator. b.operator=(a)
//    b = "This is a test"; // calls overloaded constructor first to create the object then the overloaded copy assignment operator. b.operator=("This is a test")
//    cout << a.get_str() << endl;
//    Mystring a {"Hello"};
//    a = Mystring{"Hola"};
//    a = "Bonjour";
//    Mystring larry {"LARRY"};
//    Mystring moe {"Moe"};
//    larry.display();
//    Mystring larry2 = -larry;
//    larry2.display();
//    cout << (larry == moe) << endl;
//    Mystring stooges = larry + " "+ moe;
//    stooges.display();

//    Mystring larry{"Larry"};
//    larry.display();
//    larry = -larry;
//    larry.display();
//    Mystring moe{"Moe"};
//    Mystring stooges;
//    cout << (larry == moe) << endl;
//    stooges = larry + moe;
//    stooges.display();
    Mystring curly;
    cout << "Enter a name: ";
    cin >> curly;
    cout << endl;
    cout << "Your name is " << curly << endl;
    return 0;
}