#include <iostream>
#include <memory>

class Test {
private:
    int data;
public:
    Test(): data{0} { std::cout << "test constructor (" << data << ")" << std::endl; }
    Test(int data) : data{data} { std::cout << "Test constructor (" << data << ")" << std::endl; }
    int get_data() const { return data; }
    ~Test() { std::cout << "Test destructor (" << data << ")" << std::endl; }
};

void func(std::shared_ptr<Test> p) {
    std::cout << "Use count: " << p.use_count() << std::endl;
}


int main() {
    std::shared_ptr<int> p1 {new int{100}};
    std::cout << "Use count: " << p1.use_count() << std::endl;
    
    std::shared_ptr<int> p2 = p1;
    std::cout << "Use count: " << p1.use_count() << std::endl;
    std::cout << "Use count: " << p2.use_count() << std::endl;
    p1.reset();
    std::cout << "Use count: " << p1.use_count() << std::endl;
    std::cout << "Use count: " << p2.use_count() << std::endl;
    
    std::cout << "==================" << std::endl;
    
    std::shared_ptr<Test> t1 = std::make_shared<Test>(100);
    std::cout << "Use count: " << t1.use_count() << std::endl;
    func(t1);
    return 0;
}