#include <iostream>

class I_Printable {
    friend std::ostream &operator<<(std::ostream &os, const I_Printable &obj);
public:
    virtual void print(std::ostream &os) const = 0;
    
    
};

std::ostream &operator<<(std::ostream &os, const I_Printable &obj) {
    obj.print(os);
    return os;
}

class Account : public I_Printable {
public:
    virtual void withdraw(double amount) {
        std::cout << "In Account::withdraw" << std::endl;
    }
    virtual void print(std::ostream &os) const override {
        os << "Account display";
    }
    virtual ~Account() {
//        std::cout << "Account destructor" << std::endl;
    }
    
};

class Savings: public Account {
public:
    virtual void withdraw(double amount) {
        std::cout << "In Savings::withdraw" << std::endl;
    }
    virtual void print(std::ostream &os) const override {
        os << "Savings display";
    }
    virtual ~Savings() {
//        std::cout << "Savings destructor" << std::endl;
    }
  
};

class Checking: public Account {
public:
    virtual void withdraw(double amount) {
        std::cout << "In Checking::withdraw" << std::endl;
    }
    virtual void print(std::ostream &os) const override {
        os << "Checking display";
    }
    virtual ~Checking() {
//        std::cout << "Checking destructor" << std::endl;
    }
};

class Trust: public Account {
public:
    virtual void withdraw(double amount) {
        std::cout << "In Trust::withdraw" << std::endl;
    }
    virtual void print(std::ostream &os) const override {
        os << "Trust display";
    }
    virtual ~Trust() {
//        std::cout << "Trust destructor" << std::endl;
    }
};

void do_withdraw(Account &account, double amount) {
    account.withdraw(amount);
}

int main() {
//    Account a;
//    std::cout << a << std::endl;
//    
//    Savings s;
//    std::cout << s << std::endl;

    Account *a = new Account();
    std::cout << *a << std::endl;
    
    Account *s = new Savings();
    std::cout << *s << std::endl;
    return 0;
}