#include <iostream>

class Account {
public:
    virtual void withdraw(double amount) {
        std::cout << "In Account::withdraw" << std::endl;
    }
    virtual ~Account() {
//        std::cout << "Account destructor" << std::endl;
    }
};

class Savings: public Account {
public:
    virtual void withdraw(double amount) {
        std::cout << "In Savings::withdraw" << std::endl;
    }
    virtual ~Savings() {
//        std::cout << "Savings destructor" << std::endl;
    }
  
};

class Checking: public Account {
public:
    virtual void withdraw(double amount) {
        std::cout << "In Checking::withdraw" << std::endl;
    }
    virtual ~Checking() {
//        std::cout << "Checking destructor" << std::endl;
    }
};

class Trust: public Account {
public:
    virtual void withdraw(double amount) {
        std::cout << "In Trust::withdraw" << std::endl;
    }
    virtual ~Trust() {
//        std::cout << "Trust destructor" << std::endl;
    }
};

void do_withdraw(Account &account, double amount) {
    account.withdraw(amount);
}

int main() {
//    Account *p1 = new Account();
//    Account *p2 = new Savings();
//    Account *p3 = new Checking();
//    Account *p4 = new Trust();
//
//    p1->withdraw(1000.0);
//    p2->withdraw(1000.0);
//    p3->withdraw(1000.0);
//    p4->withdraw(1000.0);
//    
//    delete p1;
//    delete p2;
//    delete p3;
//    delete p4;
//    
    Account a;
    Account &ref = a;
    ref.withdraw(1000.0);
    
    Trust t;
    Account &ref1 = t;
    ref1.withdraw(1000.0);
    
    Account a1;
    Savings a2;
    Trust a3;
    Checking a4;
    
    std::cout << "==============" << std::endl;
    
    do_withdraw(a1, 1000.0);
    do_withdraw(a2, 1000.0);
    do_withdraw(a3, 1000.0);
    do_withdraw(a4, 1000.0);
    return 0;
}