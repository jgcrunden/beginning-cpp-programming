#include <iostream>

using namespace std;

int main() {
    int num1 {10}; // This is initialisation, rather than assignment
    int num2 {20};
    
    num1 = 100;
    
    cout << "num1 is " << num1 << endl;
    cout << "num2 is " << num2 << endl;
    return 0;
}