#include <iostream>

using namespace std;

int main() {
    int cents {0};
    cout << "Enter number of cents: ";
    cin >> cents;
    
    if (cents < 0) {
        cout << "Must be greater than 0" << endl;
        return 0;
    }
    
    const int dollarValue = 100;
    const int quarterValue = 25;
    const int dimeValue = 10;
    const int nickelValue = 5;
    
    int dollars {}, quarters {}, dimes {}, nickels {}, pennies {}, balance {};
    dollars = cents / dollarValue;
    balance = cents % dollarValue;
    quarters = balance / quarterValue;
    balance = balance % quarterValue;
    dimes = balance / dimeValue;
    balance = balance % dimeValue;
    nickels = balance / nickelValue;
    pennies = balance % nickelValue;
    
    cout << "You entered: " << cents << " cents" << endl;
    cout << "Dollars: " << dollars << endl;
    cout << "Quarters: " << quarters << endl;
    cout << "Dimes: " << dimes << endl;
    cout << "Nickels: " << nickels << endl;
    cout << "pennies: " << pennies << endl;
    return 0;
}