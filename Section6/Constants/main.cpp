#include <iostream>

using namespace std;

int main() {
    
    cout << "Hello, welcome to Frank's Carpet Cleaning Service" << endl;
    cout << "\nHow many rooms would you like cleaned" << endl;
    
    int numberOfRooms {0};
    cin >> numberOfRooms;
    const double pricePerRoom {32.5};
    const double salesTax {0.06};
    const int estimateExpiry {30}; //days
    
    cout << "/nEstimate for carpet cleaning service" << endl;
    cout << "Number of rooms: " << numberOfRooms << endl;
    cout << "Price per room: $" << pricePerRoom << endl;
    cout << "Cost: $" << pricePerRoom * numberOfRooms << endl;
    cout << "Tax: $" << pricePerRoom * numberOfRooms * salesTax << endl;
    cout << "=========================" << endl;
    cout << "Total estimate is: $" << (pricePerRoom * numberOfRooms) + (pricePerRoom * numberOfRooms * salesTax) << endl;
    cout << "This estimate is valid for: " << estimateExpiry << " days" << endl;
    
    return 0;
}