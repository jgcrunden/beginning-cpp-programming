#include <iostream>

using namespace std;

int main() {
    char middleInital {'G'};
    cout << middleInital << endl;
    
    unsigned short examScore {55};
    cout << examScore << endl;
    
    int anotherNumber {65};
    cout << anotherNumber << endl;
    
    long aBigNumber { 2345678902 };
    cout << aBigNumber << endl;
    
    long peopleOnEarth = {7'600'000'000};
    cout << "There are about " << peopleOnEarth << " on earth" << endl;
    
    long double largeAmount {2.7e120};
    cout << largeAmount << " is a very big number." << endl;
    
    bool gameOver {true};
    
    cout << "Game over is equal to " << gameOver << endl;
    
    short value1 {3000};
    short value2 {4000};
    short product {value1 * value2};
    cout << "The sum of " << value1 << " + " << value2 << " = " << product << endl;
    
    return 0;
}