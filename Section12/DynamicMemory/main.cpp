#include <iostream>

using namespace std;

int main() {
    
    int *intPtr {nullptr};
    intPtr = new int;
    cout << intPtr << endl;
    delete intPtr;
    
//    size_t size{0};
//    double *tempPtr {nullptr};
//    cout << "How many temperatures do you need?";
//    cin >> size;
//    tempPtr = new double[size];
//    
//    cout << tempPtr << endl;
//    delete [] tempPtr;
    
    int arr[5] {4, 5, 6};
    cout << arr << endl;
    cout << &arr << endl;
    cout << *arr << endl;
    return 0;
}