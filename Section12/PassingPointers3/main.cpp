#include <iostream>
#include <vector>

using namespace std;
// stops locally changing the pointer and the values that the pointer points to
void display(const vector<string> * const v) {
    
}

void display(int *array, int sentinel) {
    while(*array != sentinel) {
        // dereferences array then increments array so it becomes array[n+1]
        cout << *array++ << " ";
    }
    cout << endl;
}

int main() {
    
    int scores[] {100, 98, 97, 79, 85, -1 };
    display(scores, -1);
    
    return 0;
}