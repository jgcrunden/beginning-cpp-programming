#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main() {
//    int *p;
//    cout << "Vale of p is " << p << endl;
//    cout << "Address of p is " << &p << endl;
//    cout << "Size of p is " << sizeof p << endl;
//    p = nullptr;
//    cout << "Vale of p is " << p << endl;
//    
//    double *p2 {};
//    unsigned long *p3 {};
//    cout << sizeof p2 << endl;
//    cout << sizeof p3 << endl;

//    int num{10};
//    cout << "Value of num: " << num << endl;
//    cout << "Size of num: " << sizeof num << endl;
//    cout << "Address of num: " << &num << endl;
//    
//    int *p;
//    cout << "Value of p: " << p << endl;
//    cout << "Size of p: " << sizeof p << endl;
//    cout << "Address of p: " << &p << endl;
//    
//    p = nullptr;
//    cout << "Value of p: " << p << endl;
//    cout << "Address of p: " << &p << endl;

    int score {10};
    double highTemp {100.7};
    
    int *scorePtr {nullptr};
    
    scorePtr = &score;
    cout << "Value of score is " << score << endl;
    cout << "Address of score is " << &score << endl;
    cout << "Value of scorePtr is " << scorePtr << endl;
    // scorePtr = &highTemp; // Compiler error
    return 0;
}