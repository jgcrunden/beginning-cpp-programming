#include <iostream>

using namespace std;

int main() {
    int scores[] {100, 95, 89 };
    cout << "Value of scores: " << scores << endl;
    
    int *scorePtr{scores};
    cout << "Score pointer value: " << scorePtr << endl;
    
    cout << "Array subscript notation" << endl;
    cout << scores[0] << endl; 
    cout << scores[1] << endl;
    cout << scores[2] << endl; 

    cout << "Pointer subscript notation" << endl;
    cout << scorePtr[0] << endl; 
    cout << scorePtr[1] << endl;
    cout << scorePtr[2] << endl;
    
    cout << "Pointer offset notation" << endl;
    cout << *scorePtr << endl; 
    cout << *(scorePtr + 1) << endl;
    cout << *(scorePtr + 2) << endl;
    
    cout << "Array offset notation" << endl;
    cout << *scores << endl; 
    cout << *(scores + 1) << endl;
    cout << *(scores + 2) << endl;
    return 0;
}