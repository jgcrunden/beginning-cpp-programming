#include <iostream>

using namespace std;

int *createArray(size_t size, int init_value = 0);
void display(const int *const array, size_t size);

int main() {
    int *my_array {nullptr};
    size_t size {};
    int init_value {};
    
    cout << "How many integers would you like to allocate: ";
    cin >> size;
    cout << "What would you like to initiate them to: ";
    cin >> init_value;
    
    my_array = createArray(size, init_value);
    display(my_array, size);
    delete [] my_array;
    
    return 0;
}

int *createArray(size_t size, int init_value) {
    int *newStorage {nullptr};
    newStorage = new int[size];
    for(size_t i{0}; i < size; i++) {
        *(newStorage + i) = init_value;
    }
    return newStorage;
}

void display(const int *const array, size_t size) {
    for(size_t i{0}; i < size; i++) {
        cout << array[i] << " ";
    }
    cout << endl;
}