#include <iostream>
#include <vector>

using namespace std;

int main() {
    int score {100};
    int *scorePtr {&score};
    cout << *scorePtr << endl;
    
    *scorePtr = 200;
    
    cout << *scorePtr << endl;
    cout << score << endl;
    
    cout << "**************************" << endl;
    
    double highTemp {100.7};
    double lowTemp {37.4};
    double *tempPtr {&highTemp};

    cout << *tempPtr << endl; 
    tempPtr = &lowTemp;
    cout << *tempPtr << endl;
    
    cout << "**************************" << endl;
    
    string name {"Frank"};
    string *stringPtr {&name};
    
    cout << *stringPtr << endl;
    name = "James";
    cout << *stringPtr << endl;
    
    cout << "**************************" << endl;
    
    vector<string> stooges {"Larry", "Moe", "Curly"};
    vector<string> *vectorPtr {nullptr};
    vectorPtr = &stooges;
    
    cout << "First stooge: " << (*vectorPtr).at(0) << endl;
    
    for(auto stooge : *vectorPtr) {
        cout << stooge << endl;
    }
    return 0;
}