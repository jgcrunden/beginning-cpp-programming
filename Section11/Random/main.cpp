#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main() {
    int randomNumber{};
    size_t count {10};
    int min {1};
    int max {6};
    
    cout << "RAND_MAX on my system is " << RAND_MAX << endl;
    // seed the random number generator to make it more random
    srand(time(nullptr));
    
    for(size_t i{0}; i <= count; i++) {
        randomNumber = rand() % max + min;
        cout << randomNumber << endl;
    }
    
    return 0;
}