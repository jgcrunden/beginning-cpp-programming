#include <iostream>
#include <vector>
using namespace std;

int main() {
    vector<int> vec {1, 2, 3};
    //----WRITE YOUR CODE BELOW THIS LINE----
    int result {0};
    for(unsigned int i {0}; i < vec.size(); i++) {
        cout << vec.at(i) << endl;
        for(unsigned int j {0}; j < vec.size(); j++) {
            cout << vec.at(j) << endl;
            if(i != j && i < j) {
                cout << vec.at(i) << " * " << vec.at(j) << " = " << vec.at(i) * vec.at(j) << endl;
                result += vec.at(i) * vec.at(j);
            }
            
        }
    }
    cout << result << endl;
    
     
    //----WRITE YOUR CODE ABOVE THIS LINE----
    //----DO NOT MODIFY THE CODE BELOW THIS LINE----
    return 0;
}