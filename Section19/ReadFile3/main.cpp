#include <iostream>
#include <fstream>

int main() {
    std::ifstream in_file;
    in_file.open("../test.txt");
    if(!in_file) {
        std::cout << "Problem opening file" << std::endl;
        return 1;
    }
    char c {};
    std::string line {};
    while(in_file.get(c)) {
        std::cout << c;;
    }
    in_file.close();
    return 0;
}