#include <iostream>
#include <fstream>

int main() {
    std::fstream in_file;
    in_file.open("../test.txt");
    if(!in_file) {
        std::cout << "Problem opening file" << std::endl;
        return 1;
    }
    std::string line {};
    while(std::getline(in_file, line)) {
        std::cout << line << std::endl;
    }
    in_file.close();
    return 0;
}