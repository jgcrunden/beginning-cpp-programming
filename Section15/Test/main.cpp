#include <iostream>

using namespace std;

int main() {
    int *ptr_myInt {nullptr};
    int myInt {5};
    ptr_myInt = &myInt;
    ptr_myInt = new int {6};
    cout << &myInt << endl;
    cout << ptr_myInt << endl;
    cout << *ptr_myInt << endl;
    delete ptr_myInt;
    cout << *ptr_myInt << endl;
//    cout << new int {7} << endl;
    return 0;
}