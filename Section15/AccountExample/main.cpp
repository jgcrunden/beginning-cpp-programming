#include <iostream>
#include "Account.h"
#include "SavingsAccount.h"

using namespace std;

int main() {
    Account acc {};
    acc.deposit(200.0);
    acc.withdraw(100.0);
    
    Account *p_acc {nullptr};
    p_acc = new Account();
    p_acc->deposit(1000.0);
    p_acc->withdraw(50.0);
    
    SavingsAccount savings_account {};
    savings_account.deposit(400.0);
    savings_account.withdraw(30.0);
    
    Account *p_sav_acc {nullptr};
    p_sav_acc = new Account();
    p_sav_acc->deposit(200.0);
    p_sav_acc->withdraw(70.0);
    
    
    return 0;
}