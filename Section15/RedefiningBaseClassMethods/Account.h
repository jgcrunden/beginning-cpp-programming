#ifndef ACCOUNT_H
#define ACCOUNT_H
#include <iostream>

class Account
{
    friend std::ostream &operator<<(std::ostream &os, const Account &ccount);
protected:
    double balance;
public:
    Account();
    Account(double balance);
    void deposit(double amount);
    void withdraw(double amount);
    ~Account();

};

#endif // ACCOUNT_H
