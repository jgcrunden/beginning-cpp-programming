#ifndef SAVINGSACCOUNT_H
#define SAVINGSACCOUNT_H
#include "Account.h"

class SavingsAccount: public Account {

    friend std::ostream &operator<<(std::ostream &os, const SavingsAccount &account);
protected:
    double int_rate;
public:
    SavingsAccount();
    SavingsAccount(double amount, double int_rate);
    void deposit(double amount);
    // Withdraw is inherited
    
    
    
};

#endif // SAVINGSACCOUNT_H
