#include <iostream>

int main() {
    
    int favourite_number;
    
    std::cout<<"Enter your favourite number";
    std::cin >> favourite_number;
    std::cout << "Wow, that's my favourite number" << std::endl;
    std:: cout << "No really, " << favourite_number << " really is my favourite number" << std::endl;
    return 0;
}