#include <iostream>
#include <vector>

using namespace std;

int main() {
    // vector called vowels of chars. Vector length is 5 and all are initialised as zero.
//    vector <char> vowels (5);
    vector <char> vowels {'a', 'e', 'i', 'o', 'u' };
    cout << vowels[4] << endl;
    
    // vector <int> test_scores (3, 100)
    // Creates a vector of length 3 where all values are set to 100
    vector <int> test_scores {100, 99, 95 };
    cout << test_scores.at(0) << endl;
    cout << test_scores.at(1) << endl;
    cout << test_scores.at(2) << endl;
    cout << test_scores.size() << endl;
    int newTestScore {0};
//    cin >> newTestScore;
    
    test_scores.push_back(newTestScore);
    cout << test_scores.at(3) << endl;
    
//    cout << test_scores.at(10);
    
    
    // Two Dimensional Vectors
    
    vector <vector<int>> movie_ratings {
        { 1, 2, 4, 4 },
        { 1, 2, 4, 3 },
        { 1, 3, 4, 5 }
    };
    
    cout << movie_ratings[2][3] << endl;
    
    cout << movie_ratings.at(1).at(3) << endl;
    return 0;
}