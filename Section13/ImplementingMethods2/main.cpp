#include <iostream>
#include "Account.h"

using namespace std;

int main() {
    Account frank_account;
    frank_account.set_name("Frank");
    frank_account.set_balance(1000.00);
    
    if(frank_account.deposit(200.0))
        cout << "Deposit ok" << endl;
    if(frank_account.withdraw(500.0))
        cout << "Withdraw ok" << endl;
    if(frank_account.withdraw(1500.0))
        cout << "Withdraw ok" << endl;
    else
        cout << "Insufficient funds" << endl;
    return 0;
}