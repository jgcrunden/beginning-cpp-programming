#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Player {
   string name {"Player"};
   int health {100};
   int xp {3};
   
   void talk(string);
   bool isDead();
};

class Account {
    string name;
    double balance;
    
    bool deposit(double);
    bool withdraw(double);
};

int main() {
    Account frank_acount;
    Account jim_account;
    
    Player frank;
    Player hero;
    Player players[] {frank, hero};
    vector<Player> player_vec{frank};
    player_vec.push_back(hero);
    
    
    Player *enemy {nullptr};
    enemy = new Player;
    delete enemy;
    return 0;
}