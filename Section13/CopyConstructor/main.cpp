#include <iostream>

class Player {
private:
    std::string name;
    int health;
    int xp;
public:
    Player(std::string name_val = "None", int health_val = 0, int xp_val = 0);
    Player(const Player &source);
    ~Player() {
        std::cout << "Destructor called for " << name << std::
        endl; 
    }
    
    
    void set_name(std::string name_val) {
        name = name_val;
    }
    std::string get_name() {
        return name;
    }
    
    int get_health() {
        return health;
    }
    
    int get_xp() {
        return xp;
    }
};

Player::Player(std::string name_val, int health_val, int xp_val ) 
    : name {name_val}, health {health_val}, xp {xp_val} {
        std::cout << "3 args const" << std::endl;
}

//Player::Player(const Player &source)
//    :name{source.name}, health{source.health}, xp{source.xp} {
//        std::cout << "Copy constructor - made a copy of: " << source.name << std::endl;
//}
// Can also use deligation
Player::Player(const Player &source)
    :Player{source.name, source.health, source.xp} {
        std::cout << "Copy constructor - made a copy of: " << source.name << std::endl;
}

void displayPlayer(Player p) {
    std::cout << "Name: " << p.get_name() << std::endl;
    std::cout << "Health: " << p.get_health() << std::endl;
    std::cout << "XP: " << p.get_xp() << std::endl;

    
}

int main() {
//    Player hero;
//    displayPlayer(hero);
//    Player frank {"Frank", 100, 13};
    
    Player myObject;
    Player myNewObject {myObject};
    
    return 0;
}