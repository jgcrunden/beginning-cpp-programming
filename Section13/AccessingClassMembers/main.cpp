#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Player {
public:
   string name {"Player"};
   int health {100};
   int xp {3};
   
   void talk(string textToSay) {
       cout << name << " says " << textToSay << endl;
   }
   bool isDead();
};

class Account {
public:
    string name;
    double balance;
    
    bool deposit(double);
    bool withdraw(double);
};

int main() {
    Player frank;
    frank.name = "Frank";
    frank.health = 50;
    frank.xp = 12;
    frank.talk("Hello there");
    
    Player *enemy = new Player;
    (*enemy).name = "Enemy";
    (*enemy).health = 40;
    enemy->xp = 7;
    enemy->talk("I will destroy you");
    
    return 0;
}