#include <iostream>
using namespace std;

class Player {
private:
   string name {"Player"};
   int health {100};
   int xp {3};
public:
   void talk(string textToSay) {
       cout << name << " says " << textToSay << endl;
   }
   bool isDead();
};

int main() {
    Player *frank = new Player;
    frank->talk("Hello there");
    return 0;
}