#include <iostream>

class Player {
private:
    std::string name;
    int health;
    int xp;
public:
    Player();
    Player(std::string name_val);
    Player(std::string name_val, int health_val, int xp_val );
    void set_name(std::string name_val) {
        name = name_val;
    }
    std::string get_name() {
        return name;
    }
};

Player::Player()
    : Player{"None",0,0} {
        std::cout << "No args const" << std::endl;
}

Player::Player(std::string name_val) 
    : Player{name_val, 0, 0} {
        std::cout << "1 args const" << std::endl;
}

Player::Player(std::string name_val, int health_val, int xp_val ) 
    : name {name_val}, health {health_val}, xp {xp_val} {
        std::cout << "3 args const" << std::endl;
}

int main() {
    Player hero;
    Player frank {"Frank", 100, 13};
    return 0;
}