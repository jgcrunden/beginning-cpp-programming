#include <iostream>

using namespace std;

class Shallow {
private:
    int *data;
public:
    void set_data_value(int d) {
        *data = d;
    }
    int get_data_value() {
        return *data;
    }
    
    Shallow(int d);
    
    Shallow(const Shallow &source);
    
    ~Shallow();
};

Shallow::Shallow(int d) {
    data = new int;
    *data = d;
}

Shallow::Shallow(const Shallow &source)
    :data{source.data} {
    cout << "Constructor copy - shallow copy" << endl;
}

Shallow::~Shallow() {
    delete data;
    cout << "Destructor freeing data" << endl;
}

void display_shallow(Shallow s) {
    cout << s.get_data_value();
}

int main() {
    Shallow obj1(1000);
    display_shallow(obj1);
    
    Shallow obj2 {obj1};
    obj2.set_data_value(2000);
    return 0;
}