#include <iostream>
#include <cstring>
#include <cctype>

using namespace std;

int main() {
    char firstName[20] {};
    char lastName[20] {};
    char fullName[50] {};
    char temp[50] {};
//    cout << "Enter your first name: ";
//    cin >> firstName;
//    cout << "Enter your last name: ";
//    cin >> lastName;
//    
//    cout << "Hello " << firstName << ". Your first name has " << strlen(firstName) << " characters, ";
//    cout << "and your last name, " << lastName << " has " << strlen(lastName) << " characters" << endl;
//    
//    strcpy(fullName, firstName);
//    strcat(fullName, " ");
//    strcat(fullName, lastName);
//    cout << "Your full name is " << fullName << endl;

//    cout << "Enter your full name: ";
//    cin >> fullName;
//    
//    cout << "Your full name is " << fullName << endl;

    cout << "Enter your full name: ";
    cin.getline(fullName, 50);
    cout << "Your full name is " << fullName << endl;
    
    cout << "================" << endl;
    
    strcpy(temp, fullName);
    if(strcmp(temp, fullName) == 0) {
        cout << temp << " and " << fullName << " are the same" << endl;
    } else {
        cout << temp << " and " << fullName << " are different" << endl;
    }
    
    for(size_t i{0}; i < strlen(fullName); i++) {
        if(isalpha(fullName[i])) {
            fullName[i] = toupper(fullName[i]);
        }
    }
    cout << "Your name is " << fullName;
    cout << endl;
    
    if(strcmp(temp, fullName) == 0) {
        cout << temp << " and " << fullName << " are the same" << endl;
    } else {
        cout << temp << " and " << fullName << " are different" << endl;
    }
    
    cout << "Result of compaying " << temp << " and " << fullName << ": " << strcmp(temp, fullName) << endl;
    cout << "Result of compaying " << fullName << " and " << temp << ": " << strcmp(fullName, temp) << endl;
    return 0;
}