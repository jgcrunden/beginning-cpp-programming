#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

int main() {
    string s0;
    string s1 {"Apple"};
    string s2 {"Banana"};
    string s3 {"Kiwi"};
    string s4 {"apple"};
    string s5 {s1};
    string s6 {s1, 0, 3};
    string s7 (10, 'X' );
    
//    cout << s0 << endl;
//    cout << s0.length() << endl;
    
//    cout << boolalpha;
//    cout << s1 << " == " << s5 << " = " << (s1 == s5) << endl;
//    cout << s1 << " == " << s2 << " = " << (s1 == s2) << endl;
//    cout << s1 << " != " << s2 << " = " << (s1 != s2) << endl;
//    cout << s1 << " < " << s2 << " = " << (s1 < s2) << endl;
//    cout << s2 << " > " << s1 << " = " << (s2 > s1) << endl;
//    cout << s4 << " < " << s5 << " = " << (s4 < s5) << endl;
//    cout << s1 << " == " << "Apple" << " = " << (s1 == "Apple") << endl;

//    s1 = "Watermelon";
//    cout << s1 << endl;
//    s2 = s1;
//    cout << s2 << endl;
//    
//    s2[0] = 'J';
//    cout << s2 << endl;

//    s3 = "Watermelon";
//    s3 = s5 + " and " + s2 + " juice";
//    cout << s3 << endl;
//    // cannot concatenate two string c style literals together
////    s3 = "nice " + " cold " + s3 + " juice";
//    cout << s3 << endl;

//    for(size_t i {0}; i < s1.length(); i++) {
//        cout << s1.at(i) << endl;
//    }
//    
//    for(char c : s1) {
//        cout << c;
//    }
//    cout << endl;
    
//    s1 = "This is a test";
//    
//    cout << s1.substr(0, 4) << endl;
//    cout << s1.substr(10, 4);
//    cout << endl;
//    
//    s1.erase(0, 5);
//    cout << s1 << endl;
//    string full_name {};
//    cout << "Enter your full name: ";
//    getline(cin, full_name);
//    cout << "Your full name is: " << full_n
ame << endl;

    s1 = "The secret word is boo";
    string word {};
    
    cout << "Enter the word to find: ";
    cin >> word;
    
    size_t position = s1.find(word);
    if(position != string::npos) {
        cout << "Found word " << word << " at position " << position << endl;
    } else {
        cout << "Sorry " << word << " not found" << endl;I
    }
}