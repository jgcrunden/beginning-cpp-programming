#include <iostream>
#include <string>

using namespace std;
int main() {
    string word {};
    cout << "Enter string are characters: ";
    getline(cin, word);
    
    for(size_t i {0}; i < word.length(); i++) {
        size_t numOfLeadingSpaces = word.length() - (i +1);
        string spaces (numOfLeadingSpaces, ' ');
        cout << spaces;
        int j {0};
        size_t position {numOfLeadingSpaces};
        while(j >= 0) {
            cout << word.at(j);
            position++;
            if(position >= word.length()) {
                j--;
            } else {
                j++;
            }
        }
        j = 0;
        cout << endl;
    }
    
    return 0;
}